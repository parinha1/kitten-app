export function concatUniquely<T>(array0: any[], array1: any[]): any[] {
  const a0 = array0 as []
  const a1 = array1 as []
  return [...new Set([...a0, ...a1])]
}
