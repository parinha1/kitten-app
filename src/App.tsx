import { NavigationContainer } from '@react-navigation/native'
import { useNetInfo } from '@react-native-community/netinfo'
import * as React from 'react'
import { Provider } from 'react-redux'

import { RootNavigator } from './navigation/RootNavigator'
import { store } from './state/Store'
import { NoInternet } from './component/NoInternet/NoInternet'

export const App = (): React.ReactElement => {
  const netInfo = useNetInfo()
  return (
    <Provider store={store}>
      <NavigationContainer>
        {!netInfo.isInternetReachable ? <NoInternet /> : <></>}
        <RootNavigator />
      </NavigationContainer>
    </Provider>
  )
}
