import React from 'react'
import { View, StyleSheet, Text, Image, TouchableWithoutFeedback } from 'react-native'
import { KittyCardProps } from './KittyCard.interface'

export const KittyCard: React.FC<KittyCardProps> = ({ imageUrl, onPress, title }) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.container}>
        <Image style={styles.image} source={{ uri: imageUrl }} />
        <View style={styles.textContainer}>
          <Text style={styles.text}>{title}</Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  )
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
    paddingBottom: 0,
    backgroundColor: '#ccc',
    borderRadius: 13
  },
  image: {
    width: '100%',
    height: 250
  },
  textContainer: {
    marginVertical: 20
  },
  text: {
    fontSize: 22,
    textAlign: 'center'
  }
})
