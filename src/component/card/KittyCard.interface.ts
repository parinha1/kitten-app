// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface KittyCardProps {
  title: string
  imageUrl: string
  onPress?: () => void
}
