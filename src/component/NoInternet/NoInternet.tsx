import * as React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { NoInternetProps } from './NoInternet.interface'

export const NoInternet: React.FC<NoInternetProps> = (): React.ReactElement => (
  <View style={styles.container}>
    <Text style={styles.text}>Couldn't connect to the internet</Text>
  </View>
)

const styles = StyleSheet.create({
  container: { backgroundColor: 'red' },
  text: { color: 'white', textAlign: 'center' }
})
