import * as React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { EmptyListProps } from './EmptyList.interface'

export const EmptyList: React.FC<EmptyListProps> = (): React.ReactElement => (
  <View style={styles.container}>
    <Text style={styles.text}>No data</Text>
  </View>
)

const styles = StyleSheet.create({
  container: { flex: 1 },
  text: { color: '#888', textAlign: 'center' }
})
