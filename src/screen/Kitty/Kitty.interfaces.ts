import React from 'react'
import { IKittyId } from '../../state/api/kitty/type'

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface KittyProps {}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface KittyPresentationProps {
  data: IKittyId[]
  onPressKitty: (id: React.ReactText) => void
  onChangeDisplayItem: (displayedCount: number) => void
}
