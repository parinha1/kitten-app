import React from 'react'
import { View, StyleSheet, Button, FlatList, TextInput, Alert, Keyboard } from 'react-native'
import { useSelector } from 'react-redux'
import { KittyCard } from '../../component/card/KittyCard'
import { IKittyId } from '../../state/api/kitty/type'
import { selectKittyState } from '../../state/kitty/Reducer'
import { EmptyList } from '../../component/EmptyList/EmptyList'
import { KittyPresentationProps } from '.'

const Spacer = (): React.ReactElement => <View style={styles.spacing} />

export const KittyPresentation: React.FC<KittyPresentationProps> = ({ data, onChangeDisplayItem, onPressKitty }) => {
  const { byId } = useSelector(selectKittyState)
  const [input, setInput] = React.useState<string>()

  const renderItem = ({ item }: { item: IKittyId }): React.ReactElement => {
    const kitty = byId[item]
    return (
      <KittyCard
        key={kitty?.id}
        title={kitty?.name}
        imageUrl={kitty?.photoUrl}
        onPress={() => onPressKitty(kitty.id)}
      />
    )
  }

  const handleChangeDisplayItem = (amount: string): void => {
    setInput(amount)
    if (amount === '') {
      onChangeDisplayItem(100)
      return
    }
    if (isNaN(Number(amount))) {
      Alert.alert('please enter a valid digit')
      return
    }
    onChangeDisplayItem(Number(amount))
  }

  return (
    <View style={styles.container}>
      <View style={styles.col}>
        <View style={styles.row}>
          <View style={styles.buttonWrapper}>
            <Button title="30" onPress={() => handleChangeDisplayItem('30')} />
          </View>
          <View style={styles.buttonWrapper}>
            <Button title="50" onPress={() => handleChangeDisplayItem('50')} />
          </View>
          <View style={styles.buttonWrapper}>
            <Button title="100" onPress={() => handleChangeDisplayItem('100')} />
          </View>
        </View>
        <View style={styles.row}>
          <View style={styles.inputWrapper}>
            <TextInput
              keyboardType="numeric"
              placeholder="enter amount of display item (max: 100)"
              style={styles.input}
              value={input}
              onChangeText={handleChangeDisplayItem}
              onBlur={Keyboard.dismiss}
            />
          </View>
        </View>
      </View>

      <View style={styles.listContainer}>
        <FlatList
          showsVerticalScrollIndicator={false}
          ItemSeparatorComponent={Spacer}
          data={data}
          extraData={data}
          renderItem={renderItem}
          keyExtractor={(id) => id}
          ListEmptyComponent={EmptyList}
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: { flex: 1, paddingTop: 10 },
  row: { flexDirection: 'row' },
  col: { flexDirection: 'column' },
  buttonWrapper: { height: 45, flex: 1, paddingHorizontal: 10 },
  inputWrapper: { paddingHorizontal: 10, height: 45, flex: 1 },
  input: { backgroundColor: 'white', borderRadius: 12, marginBottom: 10, padding: 10 },
  listContainer: { flex: 1, paddingHorizontal: 10 },
  spacing: { height: 10 }
})
