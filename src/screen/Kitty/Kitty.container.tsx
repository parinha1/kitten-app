import React from 'react'
import { useNavigation } from '@react-navigation/native'
import { useDispatch, useSelector } from 'react-redux'
import { setKitty } from '../../state/kitty/ActionCreator'
import { Dispatch } from '../../state/Store'
import { selectKittyState } from '../../state/kitty/Reducer'
import { KittyPresentation, KittyProps } from '.'

export const KittyScreen: React.FC<KittyProps> = () => {
  const navigation = useNavigation()
  const dispatch: Dispatch = useDispatch()
  const [totalItem, setTotalItem] = React.useState<number>(100)
  const { ids } = useSelector(selectKittyState)

  React.useEffect(() => {
    dispatch(setKitty(100))
  }, [])

  const onPressKitty = (id: React.ReactText): void => {
    navigation.navigate('KittyDetailScreen', { id })
  }

  const onChangeDisplayItem = (amountToDisplay: number): void => {
    setTotalItem(Number(amountToDisplay))
  }

  return (
    <KittyPresentation
      data={ids.slice(0, totalItem)}
      onChangeDisplayItem={onChangeDisplayItem}
      onPressKitty={onPressKitty}
    />
  )
}
