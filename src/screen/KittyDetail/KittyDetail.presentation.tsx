import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { KittyDetailPresentationProps } from '.'

export const KittyDetailPresentation: React.FC<KittyDetailPresentationProps> = ({ kitty }) => {
  return (
    <View style={styles.container}>
      <Image style={styles.image} source={{ uri: kitty.photoUrl }} />
      <View style={styles.textContainer}>
        <Text style={styles.text}>{kitty.name}</Text>
        <Text style={styles.description}>{kitty.name}</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
    paddingBottom: 0,
    backgroundColor: '#ccc',
    borderRadius: 13
  },
  image: {
    width: '100%',
    height: 250
  },
  textContainer: {
    marginVertical: 20
  },
  text: {
    fontSize: 22,
    textAlign: 'center'
  },
  description: {
    fontSize: 16
  }
})
