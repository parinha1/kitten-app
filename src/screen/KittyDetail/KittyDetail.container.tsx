import { RouteProp, useRoute } from '@react-navigation/native'
import React from 'react'
import { useSelector } from 'react-redux'
import { selectKittyState } from '../../state/kitty/Reducer'
import { KittyDetailPresentation, KittyDetailProps, KittyParamList } from '.'

export const KittyDetailScreen: React.FC<KittyDetailProps> = () => {
  const route = useRoute<RouteProp<KittyParamList, 'KittyDetailScreen'>>()
  const { byId } = useSelector(selectKittyState)

  return <KittyDetailPresentation kitty={byId[route.params.id.toString()]} />
}
