import React from 'react'
import { IKitty } from '../../state/api/kitty/type'

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface KittyDetailProps {
  id: React.ReactText
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface KittyDetailPresentationProps {
  kitty: IKitty
}

export type KittyParamList = {
  KittyDetailScreen: {
    id: React.ReactText
  }
}
