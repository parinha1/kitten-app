export type ICustomStackContentParams = IKittyStackParams

export type IKittyStackParams = {
  KittyScreen: undefined
  KittyDetailScreen: undefined
}
