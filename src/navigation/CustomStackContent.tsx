import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { KittyScreen } from '../screen/Kitty'
import { KittyDetailScreen } from '../screen/KittyDetail'
import { ICustomStackContentParams } from './CustomStackContent.interface'

const Stack = createStackNavigator<ICustomStackContentParams>()

export const KittyStack = (): React.ReactElement => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="KittyScreen" component={KittyScreen} />
      <Stack.Screen name="KittyDetailScreen" component={KittyDetailScreen} />
    </Stack.Navigator>
  )
}
