import { IKittyStackParams } from './CustomStackContent.interface'

export type IMainStackNavigatorParams = {
  KittyStack: IKittyStackParams
}
