import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { IMainStackNavigatorParams } from './MainStackNavigator.interface'
import { KittyStack } from './CustomStackContent'

const Stack = createStackNavigator<IMainStackNavigatorParams>()

export const MainStackNavigator = (): React.ReactElement => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName="KittyStack">
      <Stack.Screen name="KittyStack" component={KittyStack} />
    </Stack.Navigator>
  )
}
