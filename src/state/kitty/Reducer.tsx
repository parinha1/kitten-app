import _ from 'lodash'
import { combineReducers } from 'redux'
import { concatUniquely } from '../../utils/ConcatUniquely'
import { IKittyId, IKittyById } from '../api/kitty/type'
import { Action, State } from '../Store'

export type KittyState = {
  ids: IKittyId[]
  byId: IKittyById
}

function byIdReducer(state: IKittyById = {}, action: Action): IKittyById {
  switch (action.type) {
    case 'kitty/SET_RANDOM_KITTY':
      return _.merge(state, action.payload.byId)
    default:
      return state
  }
}

function idsReducer(state: IKittyId[] = [], action: Action): IKittyId[] {
  switch (action.type) {
    case 'kitty/SET_RANDOM_KITTY':
      return concatUniquely(state, action.payload.ids)
    default:
      return state
  }
}

export const kittyReducer = combineReducers({
  ids: idsReducer,
  byId: byIdReducer
})

export function selectKittyState(state: State): KittyState {
  return state.kitty
}
