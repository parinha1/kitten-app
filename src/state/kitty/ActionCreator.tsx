import { keyByMap } from '../../utils/KeyBy'
import { getRandomInt } from '../../utils/RandomInt'
import { transformKitty } from '../api/kitty/transformer'
import { AsyncAction } from '../Store'

const kittenName = ['Tigger', 'Smokey', 'Sassy', 'Sammy', 'Oreo', 'Whisky']

export function setKitty(limit: number = 100): AsyncAction {
  return async (dispatch) => {
    const records = []
    for (let step = 0; step < limit; step++) {
      records.push(step)
    }

    const transformed = records.map((i) =>
      transformKitty({
        id: i,
        name: kittenName[Math.floor(Math.random() * kittenName.length)],
        description:
          'There are also farm cats, which are kept on farms to keep rodents away; and feral cats, which are domestic cats that live away from humans. A cat is sometimes called a kitty. A young cat is called a kitten.',
        photoUrl: `http://placekitten.com/g/${getRandomInt(100, 300)}/${getRandomInt(100, 300)}`
      })
    )

    const ids = transformed.map((x) => x.id)

    const byId = keyByMap(
      transformed,
      (x) => x.id,
      (x) => x
    )

    dispatch({
      type: 'kitty/SET_RANDOM_KITTY',
      payload: { ids, byId }
    })
  }
}
