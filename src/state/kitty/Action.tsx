import { IKittyById, IKittyId } from '../api/kitty/type'

export type KittyAction =
  | {
      type: 'kitty/USER_CHANGE_NUMBER'
    }
  | {
      type: 'kitty/SET_RANDOM_KITTY'
      payload: {
        ids: IKittyId[]
        byId: IKittyById
      }
    }
