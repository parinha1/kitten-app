/* eslint-disable @typescript-eslint/ban-types */
import { applyMiddleware, combineReducers, createStore, Store } from 'redux'
import logger from 'redux-logger'
import thunk, { ThunkAction, ThunkDispatch } from 'redux-thunk'
import { KittyAction } from './kitty/Action'
import { kittyReducer, KittyState } from './kitty/Reducer'

export type Action = KittyAction

export type State = {
  kitty: KittyState
}

export type AsyncAction = ThunkAction<Promise<void>, State, {}, Action>
export type Dispatch = ThunkDispatch<State, {}, Action>

const rootReducer = combineReducers({
  kitty: kittyReducer
})

const middlewares = __DEV__ ? [thunk, logger] : [thunk]

export const store: Store<State, Action> = createStore(rootReducer, applyMiddleware(...middlewares))
