export type IKittyId = string

export type IKitty = {
  id: IKittyId
  name: string
  description: string
  photoUrl: string
}

export interface IKittyById {
  [id: string]: IKitty
}
