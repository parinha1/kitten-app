import { IKitty } from './type'

export function transformKitty(json: any): IKitty {
  return {
    id: json.id,
    name: json.name,
    description: json.description,
    photoUrl: json.photoUrl ?? ''
  }
}
