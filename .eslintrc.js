module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2016, // https://eslint.org/docs/user-guide/configuring#top
    project: './tsconfig.json'
  },
  plugins: [
    '@typescript-eslint',
    'prettier',
    'import',
    'react',
    'react-native',
    'react-hooks',
    'sort-destructure-keys'
  ],
  extends: [
    '@react-native-community',
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react/recommended',
    'standard-react',
    'standard-with-typescript',
    'prettier/@typescript-eslint',
    'prettier',
    'prettier/react',
    'prettier/standard',
    'plugin:prettier/recommended'
  ],
  rules: {
    'sort-imports': 'off',
    'import/order': 'off',
    '@typescript-eslint/explicit-function-return-type': [
      2,
      {
        allowExpressions: true,
        allowTypedFunctionExpressions: true
      }
    ],
    '@typescript-eslint/restrict-plus-operands': 0,
    '@typescript-eslint/interface-name-prefix': 0,
    '@typescript-eslint/strict-boolean-expressions': 0,
    '@typescript-eslint/no-floating-promises': 0,
    '@typescript-eslint/default-param-last': 0,
    '@typescript-eslint/explicit-member-accessibility': 0,
    '@typescript-eslint/no-inferrable-types': 0,
    '@typescript-eslint/camelcase': 0,
    '@typescript-eslint/no-use-before-define': 0,
    '@typescript-eslint/no-explicit-any': 0,
    '@typescript-eslint/consistent-type-definitions': 0,
    'import/order': 2,
    'react-hooks/rules-of-hooks': 2,
    'react-hooks/exhaustive-deps': 0,
    'react/no-unescaped-entities': 0,
    'react-native/no-unused-styles': 2,
    'no-console': 0,
    'react/no-unused-prop-types': 0,
    'eslint-comments/no-unused-disable': 0,
    'react/prop-types': 0,
    'react/display-name': 0,
    'react/no-unused-prop-types': 0,
    '@typescript-eslint/camelcase': 0,
    '@typescript-eslint/no-redeclare': 0,
    '@typescript-eslint/strict-boolean-expressions': 0,
    '@typescript-eslint/explicit-function-return-type': [
      2,
      {
        allowExpressions: true,
        allowTypedFunctionExpressions: true
      }
    ]
  },
  overrides: [
    {
      files: ['*.js', '*.jsx', '*.ts', '*.tsx'],
      rules: {
        'react/prop-types': 'off',
        'sort-destructure-keys/sort-destructure-keys': ['error', { caseSensitive: false }]
      }
    }
  ],
  settings: {
    react: {
      version: 'detect'
    }
  },
  globals: {
    __DEV__: false,
    Request: false,
    fetch: false,
    FormData: false
  }
}
